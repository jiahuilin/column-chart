import React from "react";
import BarGroup from "../BarGroup/BarGroup";
import "./Chart.style.css";
/**
 * @param {object} sampleDate, data to be mapped into the bar chart
 */
const Chart = ({ sampleData }) => {
  // percentage scale
  const percentagePlots = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100];
  // width of the column
  const barWidth = 15;
  // margin between each column
  const barMargin = 8;
  // height of the chart
  const chartHeight = 120;
  // width of the chart
  const chartWidth = sampleData.length * (barWidth + barMargin);

  return (
    <svg
      viewBox={`0 0 ${chartWidth} ${chartHeight + 10}`}
      width="100%"
      height="80%"
    >
      <g>
        {/**
         * y-axis
         */}
        <line
          x1={-5}
          y1={10}
          x2={-5}
          y2={chartHeight}
          stroke="darkBlue"
          strokeWidth="1"
        />
        {/**
         * x-axis
         */}
        <line
          x1={-5}
          y1={120}
          x2={chartWidth}
          y2={120}
          stroke="darkBlue"
          strokeWidth="1"
        />
        {/**
         * measurement Unit
         */}
        <text className="chart-unit-label" x={-30} y={40}>
          {"PERCENTAGE (%)"}
        </text>

        {
          // scale plots
          percentagePlots.map((value, index) => {
            return (
              <text
                className="chart-value-label"
                x={-8}
                y={120 - value}
                key={index}
                alignmentBaseline="middle"
              >
                {value}
              </text>
            );
          })
        }

        {
          // bars
          sampleData.map((data, index) => {
            const barHeight = data.expense;
            return (
              <g key={index}>
                <text
                  className="chart-value-label"
                  x={index * (barWidth + barMargin) + 10}
                  y={chartHeight - barHeight - 5}
                  key={index}
                  alignmentBaseline="middle"
                >
                  {data.expense}
                </text>
                <BarGroup
                  key={data.name}
                  x={index * (barWidth + barMargin)}
                  y={chartHeight - barHeight}
                  width={barWidth}
                  height={barHeight}
                />

                {
                  // data label
                  <text
                    className="chart-value-label"
                    x={index * (barWidth + barMargin) + 11}
                    y={chartHeight + 5}
                  >
                    {data.name}
                  </text>
                }
              </g>
            );
          })
        }
      </g>
    </svg>
  );
};
export default Chart;
