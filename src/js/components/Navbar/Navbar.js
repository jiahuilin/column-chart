import React, { useState } from "react";
import Button from "../Button/Button";
import InfoIcon from "../../../images/icon-nav-ref.png";
import Popup from "../Popup/Popup";
import "./Navbar.style.css";

const Navbar = () => {
  // state of open
  const [open, setOpen] = useState(false);
  const handleClick = () => setOpen(true);
  const handleClose = () => setOpen(false);

  return (
    <div>
      <div className="navbar">
        <Button icon={InfoIcon} onClick={handleClick} />
      </div>
      <Popup open={open} onClick={handleClose} />
    </div>
  );
};

export default Navbar;
