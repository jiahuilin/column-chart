import React from "react";
import "./BarGroup.style.css";
/**
 *
 * @param {number} x, x coordination of the column
 * @param {number} y, y coordination of the column
 * @param {number} width, width of the column
 * @param {number} height, height of the column
 * @returns
 */
const BarGroup = ({ x, y, width, height }) => {
  return (
    <rect
      x={x}
      y={y}
      width={width}
      height={height}
      fill={"darkBlue"}
      className="bar-group"
    >
      <animate
        attributeName="height"
        from={0}
        to={height}
        dur="1s"
        fill="freeze"
      />
      <animate attributeName="y" from={120} to={y} dur="1s" fill="freeze" />
    </rect>
  );
};

export default BarGroup;
