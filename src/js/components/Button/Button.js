import React from "react";
import "./Button.style.css";
/**
 *
 * @param {object} icon, background image of the button
 * @param {function} onClick, event function of the button
 *
 */
const Button = ({ icon, onClick }) => {
  return (
    <button
      className="btn"
      style={{ backgroundImage: `url("${icon}")` }}
      onClick={onClick}
    ></button>
  );
};

export default Button;
