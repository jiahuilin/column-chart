import React from "react";
import "./Popup.style.css";

/**
 * Popup dialog
 * @param {bool} open,determine if dialog supposed to be displayed
 * @param {function} onClick, close the popup dialog
 */
const PopUp = ({ open, onClick }) => {
  return (
    <div>
      {open ? (
        <div className="popup-box">
          <div className="popup-container">
            <button className="close-icon" onClick={onClick}>
              x
            </button>
            <h1 className="popup-header">REFERENCES</h1>
            <span className="popup-text">
              {
                "1. Neque porro quisquam est qui dolorem ipsum quia dolor sit amet"
              }
            </span>
            <span className="popup-text">
              {
                "2. Neque porro quisquam est qui dolorem ipsum quia dolor sit amet"
              }
            </span>
          </div>
        </div>
      ) : null}
    </div>
  );
};

export default PopUp;
