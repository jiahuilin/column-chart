import React from "react";
import Navbar from "./components/Navbar/Navbar";
import Chart from "./components/Chart/Chart";
import "./App.css";
const App = () => {
  // weekly expense
  const sampleData = [
    { name: "Mon", expense: 51 },
    { name: "Tue", expense: 80 },
    { name: "Wed", expense: 5 },
    { name: "Thur", expense: 43 },
    { name: "Fri", expense: 56 },
    { name: "Sat", expense: 51 },
    { name: "Sun", expense: 51 },
  ];

  return (
    <div>
      <Navbar />
      <header className="title">
        Neque porro quisquam est qui dolorem ipsum quia dolor sit amet
      </header>
      <div className="chart-container">
        <Chart sampleData={sampleData} />
      </div>
    </div>
  );
};

export default App;
